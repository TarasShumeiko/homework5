let table = document.createElement("table");

let tr, td;
for (let i = 0; i < 30; i++) {
  tr = document.createElement("tr");
  table.appendChild(tr);

  for (let i = 0; i < 29; i++) {
    td = document.createElement("td");
    td.className = "cell";
    tr.appendChild(td);
  }
}

document.body.appendChild(table);

table.onclick = event => {
  if (event.target.className === "black-cell") {
    event.target.className = "cell";
  } else {
    event.target.className = "black-cell";
  }
  event.stopPropagation();
};

document.body.addEventListener("click", changeColor, false);

function changeColor() {
  if (table.className === "changed-cells") {
    table.className = "";
  } else {
    table.className = "changed-cells";
  }
}
